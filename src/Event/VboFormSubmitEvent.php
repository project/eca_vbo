<?php

namespace Drupal\eca_vbo\Event;

/**
 * Dispatches when a VBO config form is being submitted.
 *
 * @internal
 *   This class is not meant to be used as a public API. It is subject for name
 *   change or may be removed completely, also on minor version updates.
 */
class VboFormSubmitEvent extends VboFormEventBase {}
